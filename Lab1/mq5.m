ox = 0:0.1:10;
oy = mq5(x, 1, 7, 3, 5);

plot(ox, oy);
xlabel = ("mq5 [a=1 b=7 c=3 d=5");
ylim([-0.05 1.05])

function y = mq5 (x, a, b, c, d)
    x_length = length(x);
    y = zeros(size(x_length));
    
    for i=1:x_length
       if x(i) <= a
           y(i) = 0;
           
       elseif a < x(i) && x(i) < c
           y(i) = (x(i) - a)/(c - a);
       
       elseif c <= x(i) && x(i) <= d
           y(i) = 1;
       
       elseif d < x(i) && x(i) < b
           y(i) = (b - x(i))/(b - d);
       
       elseif x(i) >= b
           y(i) = 0;
       
       end
    end
end