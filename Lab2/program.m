lowTip = 0.05;
averTip = 0.15;
highTip = 0.25;
tipRange = highTip-lowTip;
badService = 0;
okayService = 3; 
goodService = 7;
greatService = 10;
serviceRange = greatService-badService;
badFood = 0;
greatFood = 10;
foodRange = greatFood-badFood;
servRatio = 0.5;

n=15;
food=linspace(badFood,greatFood,n);
service=linspace(badService,goodService,n);
tip=zeros(n,n);

for i=1:n
for j=1:n
    
if service(i)<okayService
    tip(i,j) = (((averTip-lowTip)/(okayService-badService)) ...
        *service(i)+lowTip)*servRatio + ...
        (1-servRatio)*(tipRange/foodRange*food(j)+lowTip);

elseif service(i)<goodService
    tip(i,j) = averTip*servRatio + (1-servRatio)* ...
        (tipRange/foodRange*food(j)+lowTip);

else
    tip(i,j) = (((highTip-averTip)/ ...
        (greatService-goodService))* ...
        (service(i)-goodService)+averTip)*servRatio + ...
        (1-servRatio)*(tipRange/foodRange*food(j)+lowTip);
end

 
end 
end

figure
surf(food,service,tip)
xlabel('food')
ylabel('service')
zlabel('tip')
title('task')
