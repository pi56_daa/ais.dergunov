FitnessFunction = @simple_fitness;
numberOfVariables = 2;
[x,fval] = ga(FitnessFunction,numberOfVariables);
fprintf('Best simple_fitness value: %g\n', fval);

a = 100; b = 1; % define constant values
FitnessFunction = @(x) parameterized_fitness(x,a,b);
numberOfVariables = 2;
[x,fval] = ga(FitnessFunction,numberOfVariables);
fprintf('Best parameterized_fitness value: %g\n', fval);

FitnessFunction = @(x) vectorized_fitness(x,100,1);
numberOfVariables = 2;
options = optimoptions(@ga,'UseVectorized',true);
[x,fval] = ga(FitnessFunction,numberOfVariables,[],[],[],[],[],[],[],options);
fprintf('Best vectorized_fitness value: %g\n', fval);
